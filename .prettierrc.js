module.exports = {
  tabWidth: 2, // 缩进字节数
  tabs: false, // 缩进不使用tab，使用空格
  semi: false, // 句尾添加分号
  singleQuote: true, // 使用单引号代替双引号
  bracketSpacing: true, // 在对象，数组括号与文字之间加空格 "{ foo: bar }"
  arrowParens: "avoid", //  (x) => {} 箭头函数参数只有一个时是否要有小括号。avoid：省略括号
  trailingComma: "none", // 多行时尽可能打印尾随逗号
  htmlWhitespaceSensitivity: "ignore" // html 中空格也会占位，影响布局，prettier 格式化的时候可能会将文本换行
};
