import Vue from 'vue'

declare global {
  interface Window {
    wx: any
  }
}

export default Vue.extend({
  created() {
    console.log('mixins', Boolean(window.wx))
  }
})
