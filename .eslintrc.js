module.exports = {
  root: true,
  globals: {
    uni: 'readonly'
  },
  env: {
    browser: true,
    node: true,
    es6: true
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/recommended',
    'plugin:prettier/recommended',
    '@vue/prettier',
    '@vue/typescript'
  ],
  plugins: ['@typescript-eslint'],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'off' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-irregular-whitespace': 'off',
    'prettier/prettier': 'error',
    'vue/singleline-html-element-content-newline': 'off',
    'vue/multiline-html-element-content-newline': 'off',
    'vue/name-property-casing': ['error', 'PascalCase'],
    'vue/html-self-closing': [
      'error',
      {
        html: { normal: 'never', void: 'always' },
        svg: 'always',
        math: 'always'
      }
    ],
    'vue/max-attributes-per-line': [
      'error',
      {
        singleline: { max: 20 },
        multiline: {
          max: 20,
          allowFirstLine: true
        }
      }
    ],
    // 缩进
    indent: [2, 2, { SwitchCase: 1 }],
    // 指定数组的元素之间要以空格隔开(,后面)， never参数：[ 之前和 ] 之后不能带空格，always参数：[ 之前和 ] 之后必须带空格
    'array-bracket-spacing': [2, 'never'],
    // 表达式不能有多余的空格 var a = [1,  2] // error
    'no-multi-spaces': 'error',
    // 禁止行尾空格
    'no-trailing-spaces': 'error',
    // 控制逗号前后的空格
    'comma-spacing': [2, { before: false, after: true }],
    // 文件末尾强制换行
    'eol-last': 2,
    // 简略箭头函数
    'arrow-parens': ['error', 'as-needed'],
    // 要求花括号内有空格 (除了 {})
    'object-curly-spacing': ['error', 'always'],
    // 操作符周围要有空格 1+2 =》1 + 2
    'space-infix-ops': 2,
    '@typescript-eslint/no-unused-vars': 2,
    // 结尾不分号
    semi: ['error', 'never'],
    // 使用单引号
    quotes: ['error', 'single']
  },
  parserOptions: {
    parser: '@typescript-eslint/parser'
  },
  overrides: [
    {
      files: ['**/__tests__/*.{j,t}s?(x)'],
      env: {
        mocha: true
      }
    }
  ]
}
